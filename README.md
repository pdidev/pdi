This is the old repository for PDI.

PDI is now hosted at https://github.com/pdidev/pdi

Releases until 1.7 are still available here at https://gitlab.maisondelasimulation.fr/pdidev/pdi/-/releases
